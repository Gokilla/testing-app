<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class InitUserController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @return bool Exit code
     */
    public function actionIndex()
    {
        $user = new User();
        $user->login = 'admin';
        $user->full_name = 'admin';
        $user->setPassword('admin');
        $user->save();

        return ExitCode::OK;
    }
}
