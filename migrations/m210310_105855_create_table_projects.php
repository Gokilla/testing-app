<?php

use yii\db\Migration;

/**
 * Class m210310_105855_create_table_projects
 */
class m210310_105855_create_table_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'price' => $this->string()->notNull(), #String для текушего удобства в функциях можно указывать тип возрашаемоего значения
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP()'),
            'end_at' => $this->dateTime()
        ]);

        $this->addForeignKey(
            'fk-projects-author_id',
            'projects',
            'author_id',
            'users',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210310_105855_create_table_projects cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210310_105855_create_table_projects cannot be reverted.\n";

        return false;
    }
    */
}
