<?php

use yii\db\Migration;

/**
 * Class m210310_105022_create_table_user
 */
class m210310_105022_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(255)->notNull(),
            'login' => $this->string(122)->unique(),
            'password' => $this->string(),
            'auth_key' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210310_105022_create_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210310_105022_create_table_user cannot be reverted.\n";

        return false;
    }
    */
}
