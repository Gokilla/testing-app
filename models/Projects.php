<?php

namespace app\models;

use Yii;
use app\models\User;
use yii\filters\AccessControl;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int $author_id
 * @property string $title
 * @property string $price
 * @property string $created_at
 * @property string|null $end_at
 *
 * @property Users $author
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'title', 'price'], 'required'],
            [['author_id'], 'integer'],
            [['created_at', 'end_at'], 'safe'],
            [['title', 'price'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'title' => 'Title',
            'price' => 'Price',
            'created_at' => 'Created At',
            'end_at' => 'End At',
        ];
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
}
